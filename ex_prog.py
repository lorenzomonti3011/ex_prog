#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 25 14:23:37 2023

@author: lorenzo
"""

MOTS = ["Programmation", "Zoo", "Soleil", "Soleil", "Robotique", "Arbre", "Maison"]


def trier_mots (liste, inverser, suppr = False) :
    
    if suppr == True :
        liste = set(liste)    #Retire les doublons                                     
        
    liste = sorted(liste,reverse = inverser)  #Tri la liste par ordre croissant ou décroissant 
    
    return liste
 
if __name__ == '__main__':    
    
    print("liste initiale: ", MOTS)
    
    mots_tri= trier_mots(MOTS, inverser=False)  #Trier dans l'ordre croissant
    print("tri croissant : ", mots_tri)
    
    mots_tri_inv = trier_mots(MOTS, inverser=True, suppr=True) #Trier dans l'ordre décroissant sans les doublons
    print("tri décroissant sans doublons : ", mots_tri_inv)